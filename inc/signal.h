/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SIGNAL_H
#define SIGNAL_H

typedef int sigset_t;

// Ignore signal
#define SIG_IGN 0

// Signals
#define SIGABRT 1
#define SIGALRM		16
#define SIGBUS		12
#define SIGCHLD 20	// Child notification
#define SIGCONT 19	// Continue
#define SIGFPE 6
#define SIGHUP 7
#define SIGILL 8
#define SIGINT 2	// Ctrl^C
#define SIGKILL 9	// Kill
#define SIGPIPE 11
#define SIGQUIT 3	// Quit
#define SIGSEGV 13
#define SIGSTOP 14
#define SIGTERM 15
#define SIGTSTP 18	// Ctrl^Z
#define SIGTTIN 17
#define SIGTTOU		10
#define SIGUSR1		4
#define SIGUSR2		5
#define SIGPOLL 21
#define SIGPROF 22
#define SIGSYS 23
#define SIGTRAP 24
#define SIGURG 25
#define SIGVTALRM 26
#define SIGXCPU 27
#define SIGXFSZ 28

#define WEXITSTATUS(x) (x>>8)
#define WNOHANG 1

// To be set correctly
#define WUNTRACED 2
#define WIFSTOPPED(status) (status & 1)
#define WIFSIGNALED(status) (status & 2)
#define WTERMSIG(status) (status & 4)

int kill (int pid, int sig);
int signal (int, void(*)(int));

#endif
