/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TERMIOS_H
#define TERMIOS_H

// Use this documentation:
// http://man7.org/linux/man-pages/man3/termios.3.html
//
// The values and naming here is pulled from the manpages for compatibility.
// Stuff that is commented out is not supported for now. (In fact most things
// are not supported).

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////

// Number of special characters
#define NCCS 32

// Input modes
#define IGNBRK	(1 << 0)
#define BRKINT	(1 << 1)
#define IGNPAR	(1 << 2)
#define PARMRK	(1 << 3)
#define INPCK	(1 << 4)
#define ISTRIP	(1 << 5)
#define INLCR	(1 << 6)
#define IGNCR	(1 << 7)
#define ICRNL	(1 << 8)
#define IUCLC	(1 << 9)
#define IXON	(1 << 10)
#define IXANY	(1 << 11)
#define IXOFF	(1 << 12)
#define IMAXBEL	(1 << 13)
#define IUTF8	(1 << 14)

// Output modes
#define OPOST	(1 << 0)
#define OLCUC	(1 << 1)
#define ONLCR	(1 << 2)
#define OCRNL	(1 << 3)
#define ONOCR	(1 << 4)
#define ONLRET	(1 << 5)
#define OFILL	(1 << 6)
#define OFDEL	(1 << 7)

// Control modes
#define CSIZE	0x0000000F
#define CS5		5
#define CS6		6
#define CS7		7
#define CS8		8
#define CSTOPB	(1 << 8)
#define CREAD	(1 << 9)
#define PARENB	(1 << 10)
#define PARODD	(1 << 11)
#define HUPCL	(1 << 12)
#define CLOCAL	(1 << 13)
#define CRTSCTS	(1 << 14)

// Local modes
#define ISIG	(1 << 0)
#define ICANON	(1 << 1)
#define ECHO	(1 << 2)
#define ECHOE	(1 << 3)
#define ECHOK	(1 << 4)
#define ECHONL	(1 << 5)
#define NOFLSH	(1 << 6)
#define TOSTOP	(1 << 7)
#define IEXTEN	(1 << 8)

// Special characters
#define VEOF		0
#define VEOL		1
#define VERASE		2
#define VINTR		3
#define VKILL		4
#define VMIN		5
#define VQUIT		6
#define VREPRINT	7
#define VSTART		8
#define VSTOP		9
#define VSUSP		10
#define VTIME		11

// Other stuff
#define TCSANOW		0
#define TCSADRAIN	1
#define TCSAFLUSH	2

// Speed
#define B0			0
#define B50			50
#define B75			75
#define B110		110
#define B134		134
#define B150		150
#define B200		200
#define B300		300
#define B600		600
#define B1200		1200
#define B1800		1800
#define B2400		2400
#define B4800		4800
#define B9600		9600
#define B19200		19200
#define B38400		38400
#define B57600		57600
#define B115200		115200
#define B230400		230400
#define B460800		460800
#define B500000		500000
#define B576000		576000
#define B921600		921600
#define B1000000	1000000
#define B1152000	1152000
#define B1500000	1500000
#define B2000000	2000000
#define B2500000	2500000
#define B3000000	3000000
#define B3500000	3500000
#define B4000000	4000000

// Queue selector
#define TCIOFLUSH	0

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
struct termios
{
	// Standard stuff for the user
	unsigned int c_iflag;		// Input modes
	unsigned int c_oflag;		// Output modes
	unsigned int c_cflag;		// Control modes
	unsigned int c_lflag;		// Local modes
	unsigned char c_cc[NCCS];	// Special characters

	// Non-standard stuff
	unsigned int baudrate;
};

typedef unsigned int speed_t ;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

// Functions that do things with the tty
int tcgetattr (int fd, struct termios *t);
int tcsetattr (int fd, int optional_actions, const struct termios *t);
int tcsendbreak (int fd, int duration);
int tcdrain (int fd);
int tcflush (int fd, int queue_selector);
int tcflow (int fd, int action);

// Helper functions only
void cfmakeraw (struct termios *t);
unsigned int cfgetispeed (const struct termios *t);
unsigned int cfgetospeed (const struct termios *t);
int cfsetispeed (struct termios *t, unsigned int speed);
int cfsetospeed (struct termios *t, unsigned int speed);
int cfsetspeed (struct termios *t, unsigned int speed);

#endif
