/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

.syntax unified
.cpu cortex-m4
.fpu softvfp
.thumb

.word errno

/* _open =========================== */
/* _close ========================== */
/* _read =========================== */
/* _write ========================== */
/* ioctl =========================== */
.section .text._open
.global _open
.type _open, %function
_open:
	ldr r3, =errno
	svc 0
	bx lr
	nop
.size _open, .-_open

.section .text._close
.global _close
.type _close, %function
_close:
	ldr r3, =errno
	svc 1
	bx lr
	nop
.size _close, .-_close

.section .text._read
.global _read
.type _read, %function
_read:
	ldr r3, =errno
	svc 2
	bx lr
	nop
.size _read, .-_read

.section .text._write
.global _write
.type _write, %function
_write:
	ldr r3, =errno
	svc 3
	bx lr
	nop
.size _write, .-_write

.section .text._ioctl
.global _ioctl
.type _ioctl, %function
_ioctl:
	ldr r3, =errno
	svc 4
	bx lr
	nop
.size _ioctl, .-_ioctl

/* _lseek ========================== */
/* _fstat ========================== */
.section .text._lseek
.global _lseek
.type _lseek, %function
_lseek:
	ldr r3, =errno
	svc 5
	bx lr
	nop
.size _lseek, .-_lseek

.section .text._fstat
.global _fstat
.type _fstat, %function
_fstat:
	ldr r3, =errno
	svc 6
	bx lr
	nop
.size _fstat, .-_fstat

/* _stat =========================== */
/* _link =========================== */
/* _unlink ========================= */
/* rename ========================== */
/* mkdir =========================== */
/* _opendir ======================== */
/* _closedir ======================= */
/* _readdir ======================== */
/* statvfs ========================= */
.section .text._stat
.global _stat
.type _stat, %function
_stat:
	ldr r3, =errno
	svc 7
	bx lr
	nop
.size _stat, .-_stat

.section .text._link
.global _link
.type _link, %function
_link:
	ldr r3, =errno
	svc 8
	bx lr
	nop
.size _link, .-_link

.section .text._unlink
.global _unlink
.type _unlink, %function
_unlink:
	ldr r3, =errno
	svc 9
	bx lr
	nop
.size _unlink, .-_unlink

.section .text._rename
.global _rename
.type _rename, %function
_rename:
	ldr r3, =errno
	svc 10
	bx lr
	nop
.size _rename, .-_rename

.section .text.mkdir
.global mkdir
.type mkdir, %function
mkdir:
	ldr r3, =errno
	svc 11
	bx lr
	nop
.size mkdir, .-mkdir

.section .text._opendir
.global _opendir
.type _opendir, %function
_opendir:
	ldr r3, =errno
	svc 12
	bx lr
	nop
.size _opendir, .-_opendir

.section .text._closedir
.global _closedir
.type _closedir, %function
_closedir:
	ldr r3, =errno
	svc 13
	bx lr
	nop
.size _closedir, .-_closedir

.section .text._readdir
.global _readdir
.type _readdir, %function
_readdir:
	ldr r3, =errno
	svc 14
	bx lr
	nop
.size _readdir, .-_readdir

.section .text.statvfs
.global statvfs
.type statvfs, %function
statvfs:
	ldr r3, =errno
	svc 15
	bx lr
	nop
.size statvfs, .-statvfs

/* mount =========================== */
/* unmount ========================= */

/* _exit =========================== */
/* _getpid ========================= */
/* ProgRegSig ====================== */
/* ProgTrampoline ================== */
/* getcwd ========================== */
/* chdir =========================== */
/* _dup2 =========================== */
/* yield =========================== */
/* getpgrp ========================= */
/* setpgid ========================= */
.section .text._exit
.global _exit
.type _exit, %function
_exit:
	svc 18
	loopforever:
	b loopforever
	nop
.size _exit, .-_exit

.section .text._getpid
.global _getpid
.type _getpid, %function
_getpid:
	ldr r3, =errno
	svc 19
	bx lr
	nop
.size _getpid, .-_getpid

.section .text.ProgRegSig
.global ProgRegSig
.type ProgRegSig, %function
ProgRegSig:
	ldr r3, =errno
	svc 20
	bx lr
	nop
.size ProgRegSig, .-ProgRegSig

.section .text.ProgTrampoline
.global ProgTrampoline
.type ProgTrampoline, %function
ProgTrampoline:
	svc 21
	nop
.size ProgTrampoline, .-ProgTrampoline

.section .text.getcwd
.global getcwd
.type getcwd, %function
getcwd:
	ldr r3, =errno
	svc 22
	bx lr
	nop
.size getcwd, .-getcwd

.section .text.chdir
.global chdir
.type chdir, %function
chdir:
	ldr r3, =errno
	svc 23
	bx lr
	nop
.size chdir, .-chdir

.section .text._dup2
.global _dup2
.type _dup2, %function
_dup2:
	ldr r3, =errno
	svc 24
	bx lr
	nop
.size _dup2, .-_dup2

.section .text.yield
.global yield
.type yield, %function
yield:
	svc 25
	bx lr
	nop
.size yield, .-yield

.section .text.getpgrp
.global getpgrp
.type getpgrp, %function
getpgrp:
	ldr r3, =errno
	svc 26
	bx lr
	nop
.size getpgrp, .-getpgrp

.section .text.setpgid
.global setpgid
.type setpgid, %function
setpgid:
	ldr r3, =errno
	svc 27
	bx lr
	nop
.size setpgid, .-setpgid

/* _times ========================== */
/* nanosleep ======================= */
/* gettimeofday ==================== */
.section .text._times
.global _times
.type _times, %function
_times:
	ldr r3, =errno
	svc 28
	bx lr
	nop
.size _times, .-_times

.section .text.nanosleep
.global nanosleep
.type nanosleep, %function
nanosleep:
	ldr r3, =errno
	svc 29
	bx lr
	nop
.size nanosleep, .-nanosleep

.section .text.gettimeofday
.global gettimeofday
.type gettimeofday, %function
gettimeofday:
	ldr r3, =errno
	svc 30
	bx lr
	nop
.size gettimeofday, .-gettimeofday

/* asm_posix_spawn ================= */
/* waitpid ========================= */
/* _kill =========================== */
.section .text.asm_posix_spawn
.global asm_posix_spawn
.type asm_posix_spawn, %function
asm_posix_spawn:
	ldr r3, =errno
	svc 31
	bx lr
	nop
.size asm_posix_spawn, .-asm_posix_spawn

.section .text.waitpid
.global waitpid
.type waitpid, %function
waitpid:
	ldr r3, =errno
	svc 32
	bx lr
	nop
.size waitpid, .-waitpid

.section .text._kill
.global _kill
.type _kill, %function
_kill:
	ldr r3, =errno
	svc 33
	bx lr
	nop
.size _kill, .-_kill
