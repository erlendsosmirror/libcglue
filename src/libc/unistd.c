/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
char **environ;

///////////////////////////////////////////////////////////////////////////////
// Assembly functions
///////////////////////////////////////////////////////////////////////////////
int _dup2 (int, int);
void _exit (int);

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int dup (int fd)
{
	return dup2 (fd, -1);
}

int dup2 (int f1, int f2)
{
	return _dup2 (f1, f2);
}

void exit (int code)
{
	_exit (code);
}

int _isatty (int file)
{
	return ioctl (file, TISATTY, 0);
}

int pipe2 (int *pipefd, int flags)
{
	int fd1 = open ("/pipe/unnamed", O_RDWR | flags);

	if (fd1 < 0)
		return -1;

	int fd2 = dup (fd1);

	if (fd2 < 0)
	{
		close (fd1);
		return -2;
	}

	pipefd[0] = fd1;
	pipefd[1] = fd2;
	return 0;
}

int pipe (int *pipefd)
{
	return pipe2 (pipefd, 0);
}

int rmdir (const char *path)
{
	return unlink (path);
}

pid_t tcgetpgrp (int fileno)
{
	return ioctl (fileno, TCGETPGRP, 0);
}

int tcsetpgrp (int fileno, pid_t pgrp)
{
	return ioctl (fileno, TCSETPGRP, pgrp);
}
