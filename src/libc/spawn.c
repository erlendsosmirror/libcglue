/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <spawn.h>

///////////////////////////////////////////////////////////////////////////////
// External functions
///////////////////////////////////////////////////////////////////////////////
int asm_posix_spawn (pid_t *pid, const char *name, int *args);

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
__attribute__((optimize("1")))
int posix_spawn (pid_t *pid, const char *name,
	const posix_spawn_file_actions_t *factions, const posix_spawnattr_t *attr, char **argv, char **env)
{
	int arg[4] = {(int)factions, (int)attr, (int)argv, (int)env};
	return asm_posix_spawn (pid, name, arg);
}

int posix_spawnattr_init (posix_spawnattr_t *attr)
{
	attr->flags = 0;
	attr->pgroup = 0;
	return 0;
}

int posix_spawnattr_setflags (posix_spawnattr_t *attr, int flags)
{
	attr->flags |= flags;
	return 0;
}

int posix_spawnattr_setpgroup (posix_spawnattr_t *attr, int pgroup)
{
	attr->pgroup = pgroup;
	return 0;
}

int posix_spawnattr_destroy (posix_spawnattr_t *attr)
{
	return 0;
}

int posix_spawn_file_actions_init (posix_spawn_file_actions_t *factions)
{
	memset (factions, 0, sizeof (posix_spawn_file_actions_t));
	factions->nfileactions = POSIX_SPAWN_NFACTIONS;
	return 0;
}

int posix_spawn_file_actions_addopen (posix_spawn_file_actions_t *factions, int fildes, char *path, int oflag, int mode)
{
	for (int i = 0; i < POSIX_SPAWN_NFACTIONS; i++)
	{
		if (!factions->fa[i].inuse)
		{
			factions->fa[i].fildes = fildes;
			factions->fa[i].path = path;
			factions->fa[i].oflag = oflag;
			factions->fa[i].mode = mode;
			factions->fa[i].inuse = path ? 1 : 2;
			return 0;
		}
	}

	return 1;
}

int posix_spawn_file_actions_adddup2 (posix_spawn_file_actions_t *factions, int fildes, int newfildes)
{
	return posix_spawn_file_actions_addopen (factions, fildes, 0, newfildes, 0);
}

int posix_spawn_file_actions_destroy (posix_spawn_file_actions_t *factions)
{
	return 0;
}
