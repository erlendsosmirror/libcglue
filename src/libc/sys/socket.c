/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "sys/socket.h"
#include "errno.h"

///////////////////////////////////////////////////////////////////////////////
// Asm functions
///////////////////////////////////////////////////////////////////////////////
int bind_asm (int fd, const struct sockaddr *addr, socklen_t addrlen, int *errno);
int recvfrom_asm (int fd, int *args, int unused, int *errno);
int sendto_asm (int fd, int *args, int unused, int *errno);
int socket_asm (int af, int sock, int ipproto, int *errno);

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
// TODO
int accept (int, struct sockaddr *restrict, socklen_t *restrict);

int bind (int fd, const struct sockaddr *addr, socklen_t addrlen)
{
	return bind_asm (fd, addr, addrlen, &errno);
}

// TODO
int connect (int, const struct sockaddr *, socklen_t);
int getpeername (int, struct sockaddr *restrict, socklen_t *restrict);
int getsockname (int, struct sockaddr *restrict, socklen_t *restrict);
int getsockopt (int, int, int, void *restrict, socklen_t *restrict);
int listen (int, int);

ssize_t recv (int fd, void *buf, size_t len, int flags)
{
	int args[] = {(int)buf, (int)len, flags, 0, 0};
	return (ssize_t) recvfrom_asm (fd, args, 0, &errno);
}

ssize_t recvfrom (int fd, void *restrict buf, size_t len, int flags, struct sockaddr *restrict addr, socklen_t *restrict addrlen)
{
	int args[] = {(int)buf, (int)len, flags, (int)addr, (int)addrlen};
	return (ssize_t) recvfrom_asm (fd, args, 0, &errno);
}

// TODO
ssize_t recvmsg (int, struct msghdr *, int);

ssize_t send (int fd, const void *buf, size_t len, int flags)
{
	int args[] = {(int)buf, (int)len, flags, 0, 0};
	return (ssize_t) sendto_asm (fd, args, 0, &errno);
}

// TODO
ssize_t sendmsg (int, const struct msghdr *, int);

ssize_t sendto (int fd, const void *buf, size_t len, int flags, const struct sockaddr *addr, socklen_t addrlen)
{
	int args[] = {(int)buf, (int)len, flags, (int)addr, (int)addrlen};
	return (ssize_t) sendto_asm (fd, args, 0, &errno);
}

// TODO
int setsockopt (int, int, int, const void*, socklen_t);
int shutdown (int, int);
int sockatmark (int);

int socket (int af, int sock, int ipproto)
{
	return socket_asm (af, sock, ipproto, &errno);
}

// TODO
int socketpair (int, int, int, int[2]);
