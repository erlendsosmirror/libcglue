/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// External functions
///////////////////////////////////////////////////////////////////////////////
void _itoa (int n, char *str);
void itoa_uint32_hex (unsigned int in, char *out);

///////////////////////////////////////////////////////////////////////////////
// Internal functions
///////////////////////////////////////////////////////////////////////////////
int puts_internal (const char *s)
{
	// Get length
	int len = strlen (s);

	// Working vars
	int n = len;
	int pos = 0;

	// Write while there is more data
	do
	{
		int diff = write (1, s + pos, n);

		if (diff < 0)
			return diff;

		pos += diff;
		n -= diff;
	} while (pos != len);

	return len;
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int fprintf (FILE *file, const char *format, ...)
{
	va_list list;
	va_start (list, format);
	int ret = printf (format, list);
	va_end (list);
	return ret;
}

int printf (const char *format, ...)
{
	// Initialize argument list
	va_list list;
	va_start (list, format);

	// Initialize output data and duplicate a pointer
	char output[128];
	memset (output, 0, sizeof (output));

	// Process the format string
	for (char *out = output; *format; format++)
	{
		if (*format == '%')
		{
			format++;
			int modifier = -1;

			if (*format == '.')
			{
				format++;
				modifier = *format - '0';

				if (!*format)
					break;

				format++;
			}

			if (*format == 'i')
			{
				int arg = va_arg (list, int);
				_itoa (arg, out);
			}
			else if (*format == 'x' || *format == 'X')
			{
				unsigned int arg = va_arg (list, unsigned int);
				char str[16];
				char *st = str;
				itoa_uint32_hex (arg, str);

				if (modifier > 0)
				{
					modifier = 8 - modifier;

					if (modifier > 0 && modifier < 8)
						st += modifier;
				}

				while (*st)
					*out++ = *st++;
			}
			else if (*format == 'c')
			{
				char c = va_arg (list, int);
				*out = c;
			}
			else if (*format == 's')
			{
				char *s = va_arg (list, char*);
				while (*s)
					*out++ = *s++;
			}
			else
				*out = *format;
		}
		else
			*out = *format;

		while (*out) out++;
	}

	// Print the string and exit
	va_end (list);
	return puts_internal (output);
}

int puts (const char *s)
{
	int ret = puts_internal (s);

	if (ret < 0)
		return ret;

	return ret + puts_internal ("\n");
}
