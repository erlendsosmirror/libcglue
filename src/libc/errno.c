/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <errno.h>

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
#undef errno
int errno;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

// Note: Using the first option here saves about a kilobyte, but the messages
// are less descriptive.
#if 0
char *strerror (int err)
{
	switch (err)
	{
	case E2BIG: return "E2BIG";
	case EACCES: return "EACCES";
	case EADDRINUSE: return "EADDRINUSE";
	case EADDRNOTAVAIL: return "EADDRNOTAVAIL";
	case EAFNOSUPPORT: return "EAFNOSUPPORT";
	case EAGAIN: return "EAGAIN";
	case EALREADY: return "EALREADY";
	case EBADF: return "EBADF";
	case EBADMSG: return "EBADMSG";
	case EBUSY: return "EBUSY";
	case ECANCELED: return "ECANCELED";
	case ECHILD: return "ECHILD";
	case ECONNABORTED: return "ECONNABORTED";
	case ECONNREFUSED: return "ECONNREFUSED";
	case ECONNRESET: return "ECONNRESET";
	case EDEADLK: return "EDEADLK";
	case EDESTADDRREQ: return "EDESTADDRREQ";
	case EDOM: return "EDOM";
	case EDQUOT: return "EDQUOT";
	case EEXIST: return "EEXIST";
	case EFAULT: return "EFAULT";
	case EFBIG: return "EFBIG";
	case EHOSTUNREACH: return "EHOSTUNREACH";
	case EIDRM: return "EIDRM";
	case EILSEQ: return "EILSEQ";
	case EINPROGRESS: return "EINPROGRESS";
	case EINTR: return "EINTR";
	case EINVAL: return "EINVAL";
	case EIO: return "EIO";
	case EISCONN: return "EISCONN";
	case EISDIR: return "EISDIR";
	case ELOOP: return "ELOOP";
	case EMFILE: return "EMFILE";
	case EMLINK: return "EMLINK";
	case EMSGSIZE: return "EMSGSIZE";
	case EMULTIHOP: return "EMULTIHOP";
	case ENAMETOOLONG: return "ENAMETOOLONG";
	case ENETDOWN: return "ENETDOWN";
	case ENETRESET: return "ENETRESET";
	case ENETUNREACH: return "ENETUNREACH";
	case ENFILE: return "ENFILE";
	case ENOBUFS: return "ENOBUFS";
	case ENODATA: return "ENODATA";
	case ENODEV: return "ENODEV";
	case ENOENT: return "ENOENT";
	case ENOEXEC: return "ENOEXEC";
	case ENOLCK: return "ENOLCK";
	case ENOLINK: return "ENOLINK";
	case ENOMEM: return "ENOMEM";
	case ENOMSG: return "ENOMSG";
	case ENOPROTOOPT: return "ENOPROTOOPT";
	case ENOSPC: return "ENOSPC";
	case ENOSR: return "ENOSR";
	case ENOSTR: return "ENOSTR";
	case ENOSYS: return "ENOSYS";
	case ENOTCONN: return "ENOTCONN";
	case ENOTDIR: return "ENOTDIR";
	case ENOTEMPTY: return "ENOTEMPTY";
	case ENOTSOCK: return "ENOTSOCK";
	case ENOTSUP: return "ENOTSUP";
	case ENOTTY: return "ENOTTY";
	case ENXIO: return "ENXIO";
	case EOPNOTSUPP: return "EOPNOTSUPP";
	case EOVERFLOW: return "EOVERFLOW";
	case EPERM: return "EPERM";
	case EPIPE: return "EPIPE";
	case EPROTO: return "EPROTO";
	case EPROTONOSUPPOT: return "EPROTONOSUPPOT";
	case EPROTOTYPE: return "EPROTOTYPE";
	case ERANGE: return "ERANGE";
	case EROFS: return "EROFS";
	case ESPIPE: return "ESPIPE";
	case ESRCH: return "ESRCH";
	case ESTALE: return "ESTALE";
	case ETIME: return "ETIME";
	case ETIMEDOUT: return "ETIMEDOUT";
	case ETXTBSY: return "ETXTBSY";
	case EWOULDBLOCK: return "EWOULDBLOCK";
	case EXDEV: return "EXDEV";
	default: return "";
	}
}
#else
char *strerror (int err)
{
	switch (err)
	{
	case E2BIG: return "Argument list too long";
	case EACCES: return "Permission denied";
	case EADDRINUSE: return "Address in use";
	case EADDRNOTAVAIL: return "Address not available";
	case EAFNOSUPPORT: return "Address family not supported";
	case EAGAIN: return "Resource unavailable, try again";
	case EALREADY: return "Connection already in progress";
	case EBADF: return "Bad file descriptor";
	case EBADMSG: return "Mad message";
	case EBUSY: return "Device or resource busy";
	case ECANCELED: return "Operation canceled";
	case ECHILD: return "No child processes";
	case ECONNABORTED: return "Connection aborted";
	case ECONNREFUSED: return "Connection refused";
	case ECONNRESET: return "Connection reset";
	case EDEADLK: return "Resource deadlock would occur";
	case EDESTADDRREQ: return "Destination address required";
	case EDOM: return "Mathematics argument out of domain of function";
	case EDQUOT: return "Reserved";
	case EEXIST: return "File exists";
	case EFAULT: return "Bad address";
	case EFBIG: return "File too large";
	case EHOSTUNREACH: return "Host is unreachable";
	case EIDRM: return "Identifier removed";
	case EILSEQ: return "Illegal byte sequence";
	case EINPROGRESS: return "Operation in progress";
	case EINTR: return "Interrupted function";
	case EINVAL: return "Invalid argument";
	case EIO: return "I/O error";
	case EISCONN: return "Socket is connected";
	case EISDIR: return "Is a directory";
	case ELOOP: return "Too many levels of symbolic links";
	case EMFILE: return "Too many open files";
	case EMLINK: return "Too many links";
	case EMSGSIZE: return "Message too large";
	case EMULTIHOP: return "Reserved";
	case ENAMETOOLONG: return "Filename too long";
	case ENETDOWN: return "Network is down";
	case ENETRESET: return "Connection aborted by network";
	case ENETUNREACH: return "Network unreachable";
	case ENFILE: return "Too many files open in system";
	case ENOBUFS: return "No buffer space available";
	case ENODATA: return "No message is available";
	case ENODEV: return "No such device";
	case ENOENT: return "No such file or directory";
	case ENOEXEC: return "Executable file format error";
	case ENOLCK: return "No locks available";
	case ENOLINK: return "Reserved";
	case ENOMEM: return "Not enough space";
	case ENOMSG: return "No message of the desired type";
	case ENOPROTOOPT: return "Protocol not available";
	case ENOSPC: return "No space left on device";
	case ENOSR: return "No STREAM resources";
	case ENOSTR: return "Not a STREAM";
	case ENOSYS: return "Function not supported";
	case ENOTCONN: return "The socket is not connected";
	case ENOTDIR: return "Not a directory";
	case ENOTEMPTY: return "Directory not empty";
	case ENOTSOCK: return "Not a socket";
	case ENOTSUP: return "Not supported";
	case ENOTTY: return "Inappropriate I/O control operation";
	case ENXIO: return "No such device or address";
	case EOPNOTSUPP: return "Operation not supported on socket";
	case EOVERFLOW: return "Value too large to be stored in data type";
	case EPERM: return "Operation not permitted";
	case EPIPE: return "Broken pipe";
	case EPROTO: return "Protocol error";
	case EPROTONOSUPPOT: return "Protocol not supported";
	case EPROTOTYPE: return "Protocol wrong type for socket";
	case ERANGE: return "Result too large";
	case EROFS: return "Read-only file system";
	case ESPIPE: return "Invalid seek";
	case ESRCH: return "No such process";
	case ESTALE: return "Reserved";
	case ETIME: return "Stream ioctl timeout";
	case ETIMEDOUT: return "Connection timed out";
	case ETXTBSY: return "Text file busy";
	case EWOULDBLOCK: return "Operation would block";
	case EXDEV: return "Cross-device link";
	default: return "";
	}
}
#endif
