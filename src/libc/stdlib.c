/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct Block
{
	struct Block *prev;
	struct Block *next;
	unsigned int size; // Includes block header (sizeof Block)
	int status;
} Block;

union Alloc
{
	unsigned int data[(1*1024)/4];
	Block super;
};

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
//union Alloc allocpool;
extern union Alloc _heap_start;
extern union Alloc _heap_end;
size_t currentfree;
size_t minfree;

///////////////////////////////////////////////////////////////////////////////
// Helper functions
///////////////////////////////////////////////////////////////////////////////
void *AllocateFrom (Block *b, unsigned int size)
{
	unsigned int sizea = sizeof (Block) + size;
	unsigned int sizeb = b->size - sizea;

	char *p = (char*) b;
	p += sizea;
	Block *n = (Block*) p;

	n->prev = b;
	n->next = b->next;
	n->size = sizeb;
	n->status = 0;

	b->next = n;
	b->size = sizea;
	b->status = 1;

	currentfree -= sizea;

	if (currentfree < minfree)
		minfree = currentfree;

	return (char*)b + sizeof (Block);
}

void Concat (Block *a, Block *b)
{
	a->next = b->next;
	a->size += b->size;

	if (b->next)
		b->next->prev = a;
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void *malloc (size_t size)
{
	static int initialized;

	if (!initialized)
	{
		int a = (int) &_heap_start;
		int b = (int) &_heap_end;
		int size = b - a;

		_heap_start.super.prev = 0;
		_heap_start.super.next = 0;
		_heap_start.super.size = size;
		_heap_start.super.status = 0;
		initialized = 1;
		currentfree = size;
		minfree = currentfree;
	}

	size = (size + 3) & ~3;

	for (Block *b = &_heap_start.super; b; b = b->next)
	{
		unsigned int sub = sizeof (Block) * 2;

		if (b->size < sub)
			continue;

		unsigned int space = b->size - sub;

		if (!b->status && space >= size)
			return AllocateFrom (b, size);
	}

	return 0;
}

void free (void *mem)
{
	if (!mem)
		return;

	char *p = (char*) mem;
	p -= sizeof (Block);
	Block *b = (Block*) p;

	b->status = 0;

	currentfree += b->size;

	if (b->prev && b->prev->status == 0 && b->next && b->next->status == 0)
	{
		Concat (b->prev, b);
		Concat (b->prev, b->next);
	}
	else if (b->prev && b->prev->status == 0)
		Concat (b->prev, b);
	else if (b->next && b->next->status == 0)
		Concat (b, b->next);
}

size_t malloc_getminfree (void)
{
	return minfree;
}
