###############################################################################
# Project name and files
###############################################################################
PROJECT_NAME = libcglue
NOLIBCGLUE = true
PROJECT_FILES += $(shell find src/libc -name "*.*")

include ../../kernel/kernel/config/live/arch

ifeq ($(ARCH),tms570ls1224)
PROJECT_FILES += $(shell find src/arch/cortex-r -name "*.*")
else
PROJECT_FILES += $(shell find src/arch/cortex-m -name "*.*")
endif

###############################################################################
# Compiler flags, file processing and standard targets
###############################################################################
include ../../util/build/makefile/flags
include ../../util/build/makefile/processfiles
include ../../util/build/makefile/targets

###############################################################################
# Makefile execution
###############################################################################
all: build/$(PROJECT_NAME).a

build/$(PROJECT_NAME).a: $(OBJECTS)
	@arm-none-eabi-ar rcs $@ $(OBJECTS)
